//
//  NSString+Extend.h
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Extend)
+(BOOL)handleStringIsNull:(id )string;
@end

NS_ASSUME_NONNULL_END
