//
//  UIView+Frame.h
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Frame)


@property(nonatomic ,assign) CGFloat X;

@property(nonatomic ,assign) CGFloat Y;

@property(nonatomic ,assign) CGFloat Width;

@property(nonatomic ,assign) CGFloat High;

@property(nonatomic ,assign) CGFloat MaxWidth;

@property(nonatomic ,assign) CGFloat MaxHigh;

- (void)setMj_x:(CGFloat)mj_x;

- (void)setMj_y:(CGFloat)mj_y;

-(void)setMj_h:(CGFloat)mj_h;

- (void)setMj_w:(CGFloat)mj_w;

@end

NS_ASSUME_NONNULL_END
