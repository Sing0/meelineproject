//
//  UIView+Frame.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)


- (void)setMj_x:(CGFloat)mj_x
{
    CGRect frame = self.frame;
    frame.origin.x = mj_x;
    self.frame = frame;
}

//- (CGFloat)mj_x
//{
//    return self.frame.origin.x;
//}

- (void)setMj_y:(CGFloat)mj_y
{
    CGRect frame = self.frame;
    frame.origin.y = mj_y;
    self.frame = frame;
}

//- (CGFloat)mj_y
//{
//    return self.frame.origin.y;
//}

-(void)setMj_h:(CGFloat)mj_h{
    CGRect frame = self.frame;
    frame.size.height = mj_h;
    self.frame = frame;
}



- (void)setMj_w:(CGFloat)mj_w
{
    CGRect frame = self.frame;
    frame.size.width = mj_w;
    self.frame = frame;
}

-(CGFloat)X{
    return self.frame.origin.x;
}

-(CGFloat)Y{
    return self.frame.origin.y;
}

-(CGFloat)Width{
    return self.bounds.size.width;
}

-(CGFloat)High{
    return self.bounds.size.height;
}

-(CGFloat)MaxWidth{
    return self.X + self.Width;
}

-(CGFloat)MaxHigh{
    return self.Y + self.High;
}

@end
