//
//  MLHeadLabel.m
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLHeadLabel.h"

@implementation MLHeadLabel


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor ColorWithHexString:@"#E7EAEC"];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = self.Width/2.0;
        self.textAlignment = NSTextAlignmentCenter;
        self.font = [UIFont systemFontOfSize:12.f*kWidth_Scale];
        self.textColor = [UIColor ColorWithHexString:@"#6A7194"];
    } return self;
}


@end
