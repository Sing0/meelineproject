//
//  MLNumberLabel.m
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLNumberLabel.h"

@implementation MLNumberLabel

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kReadColor;
        self.textAlignment = NSTextAlignmentCenter;
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 10*kWidth_Scale;
        self.font = [UIFont systemFontOfSize:15.f*kWidth_Scale];
        self.textColor = [UIColor whiteColor];
    } return self;
}

-(void)setNumber:(NSString*)number{
    
    self.alpha = 0;
    if ([NSString handleStringIsNull:number]) {
        NSInteger count = [number integerValue];
        if (count > 0) {
             self.alpha = 1;
            if (count<10) {
                [self setMj_w:20*kWidth_Scale];
            }else if (count<100){
                [self setMj_w:27*kWidth_Scale];
//                [self setMj_x:4*kWidth_Scale];
            }else{
                [self setMj_w:35*kWidth_Scale];
            }
            if (count > 99) {
                number = @"99+";
            }
            self.text = number;
        }
    }
}


@end
