//
//  MLNumberLabel.h
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLNumberLabel : UILabel
-(void)setNumber:(NSString*)number;
@end

NS_ASSUME_NONNULL_END
