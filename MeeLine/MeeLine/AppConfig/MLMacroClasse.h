//
//  MLMacroClasse.h
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//



#pragma mark - 尺度相关

///自读屏宽高
#define KSCREEN_WIDTH   [UIScreen mainScreen].bounds.size.width
#define KSCREEN_HEIGHT  [UIScreen mainScreen].bounds.size.height

///判断手机是否为iPhone X 及其以上机型（根据屏幕长度来进行判断）
#define iPhoneX      KSCREEN_HEIGHT >= 812 ? YES : NO

// 手机屏幕顶部高度（导航栏和状态兰高度）
#define kNavi_StaBarHeight ((iPhoneX == YES)?(88.0):(64.0))
// 手机底部高度
#define kTabBarHeight      ((iPhoneX == YES)?(49.0 + 34.0):49.0f)












//全局比例尺
#define kWidth_Scale KSCREEN_WIDTH/375.0f
// 高度比例尺
#define kHeight_Scale :CGFloat = SCREEN_HEIGHT/667.0f



#define weakSelf(type)  __weak __typeof(type) weak##type = type;
#define strongSelf(type)  __strong __typeof(type) strong##type = weak##type;if (strong##type==nil) return;



#pragma mark - 常用颜色

// 主题绿色
#define kGreenColor         [UIColor  ColorWithHexString:@"#68BB1E"]
// 主题白色（导航字体颜色）
#define kWhiteColor         [UIColor  ColorWithHexString:@"#FFFFFF"]
// 主题灰白（线）
#define kGrayWhiteColor     [UIColor  ColorWithHexString:@"#F0F0F6"]
// 主题红色
#define  kReadColor         [UIColor ColorWithHexString:@"#EB3324"]



#pragma mark - block

typedef void (^ActionBlock)(id data);

typedef void (^TypeDataBlock)(int type ,id data);





