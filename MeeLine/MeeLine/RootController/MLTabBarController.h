//
//  MLTabBarController.h
//  MeeLine
//
//  Created by sing on 2020/4/11.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
