//
//  MLTabBarController.m
//  MeeLine
//
//  Created by sing on 2020/4/11.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLTabBarController.h"
#import "MLGroupRootVc.h"
#import "MLFriendRootVc.h"
#import "MLChatRootVc.h"
#import "MLMeRootVc.h"


@interface MLTabBarController ()

@end

@implementation MLTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSLog(@"-- %@",[UIApplication sharedApplication].windows);
    
    [self addChildVc:[MLChatRootVc new]   title:@"密聊" normalImage:@"tabbar_Chat_Normal" selectedImage:@"tabbar_Chat_Selected"];
    [self addChildVc:[MLFriendRootVc new] title:@"密友" normalImage:@"tabbar_Friend_Normal" selectedImage:@"tabbar_Friend_Selected"];
    [self addChildVc:[MLGroupRootVc new]  title:@"密群" normalImage:@"tabbar_Group_Normal" selectedImage:@"tabbar_Group_Selected"];
    [self addChildVc:[MLMeRootVc new]     title:@"我的" normalImage:@"tabbar_Me_Normal" selectedImage:@"tabbar_Me_Selected"];
    
}





- (void) addChildVc:(UIViewController *)childVc title:(NSString *) title normalImage:(NSString *)normalImage selectedImage:(NSString *) selectedImage {
    
    childVc.tabBarItem.title = title;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = [UIColor ColorWithHexString:@"#2B2F40"];
//    dict[NSFontAttributeName] = [UIFont systemFontOfSize:10*kWidth_Scale];
    [childVc.tabBarItem setTitleTextAttributes:dict forState:UIControlStateNormal];
//
    NSMutableDictionary *dicts = [NSMutableDictionary dictionary];
    dicts[NSForegroundColorAttributeName] = kGreenColor; // 字体颜色
    [childVc.tabBarItem setTitleTextAttributes:dicts forState:UIControlStateSelected];


    childVc.tabBarItem.image = [[UIImage imageNamed:normalImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UINavigationController *na = [[UINavigationController alloc] initWithRootViewController:childVc];
////    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -300) forBarMetrics:UIBarMetricsDefault];
    na.navigationItem.backBarButtonItem=nil;
    na.navigationBar.tintColor = kWhiteColor;
    //na.navigationBar.backgroundColor = [UIColor ColorWithHexString:@"#131313"];
    na.navigationBar.barTintColor = [UIColor ColorWithHexString:@"#2B2F41"];
    na.navigationBar.translucent = NO;
    [na.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:17.0],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self addChildViewController:na];
}




@end
