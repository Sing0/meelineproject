//
//  MLChatRootVc.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLChatRootVc.h"
#import "MLChatRootView.h"
#import "UIBarButtonItem+XYMenu.h"

@interface MLChatRootVc ()
@property(nonatomic ,strong) MLChatRootView *chatView;
@end

@implementation MLChatRootVc

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"密聊";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeSystem];
//    [cancleButton setTitle:@"撤销" forState:UIControlStateNormal];
    [addButton setImage:[UIImage imageNamed:@"nav_add_buttonicon"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addButtonClicked) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:addButton];
//    rightItem.imageEdgeInsets = UIEdgeInsetsMake(0, -15,0, 0);//设置向左偏移
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    
    
    
    
    weakSelf(self);
    self.chatView = [[MLChatRootView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH, KSCREEN_HEIGHT-kNavi_StaBarHeight-kTabBarHeight) withBlock:^(id data) {
        strongSelf(self);
////                   [strongself viewDidLoad];
//        [weakself viewDidLoad];
    }];
    [self.view addSubview:self.chatView];
    
    
    
    // Do any additional setup after loading the view.
}



-(void)addButtonClicked{
    
    UIBarButtonItem *item = (UIBarButtonItem *)self.navigationItem.rightBarButtonItem;
       NSArray *imageArr = @[@"icon_add_friends", @"icon_add_friends", @"icon_add_friends"];
       NSArray *titleArr = @[@"新建密群", @"添加密友", @"扫一扫"];
       [item xy_showMenuWithImages:imageArr titles:titleArr menuType:XYMenuRightNavBar currentNavVC:self.navigationController withItemClickIndex:^(NSInteger index) {
           NSLog(@"%@",titleArr[index-1]);
//           [self showMessage:index];
       }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
