//
//  MLChatRootCell.h
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLChatRootModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MLChatRootCell : UITableViewCell

@property(nonatomic, strong) MLChatRootModel *model;

@end

NS_ASSUME_NONNULL_END
