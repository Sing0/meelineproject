//
//  MLChatRootView.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLChatRootView.h"
#import "MLChatRootCell.h"

@interface MLChatRootView ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,copy)   ActionBlock block;
@property(nonatomic ,strong) UISearchBar *searchBar;
@end


@implementation MLChatRootView


-(instancetype) initWithFrame:(CGRect)frame withBlock:(ActionBlock)block{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if ( self) {
        self.block = block;
        self.delegate = self;
        self.dataSource = self;
        [self registerClass:[MLChatRootCell class] forCellReuseIdentifier:@"MLChatRootCell"];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSearchBar];
    } return self;
}

-(void)addSearchBar{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.Width, 40*kWidth_Scale)];
    view.backgroundColor = kGrayWhiteColor;
    self.tableHeaderView = view;
//    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(10*kWidth_Scale, 10*kWidth_Scale, view.Width-20*kWidth_Scale, 35*kWidth_Scale)];
//    self.searchBar.backgroundColor = [UIColor whiteColor];
//    [view addSubview:self.searchBar];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65*kWidth_Scale;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MLChatRootCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MLChatRootCell" forIndexPath:indexPath];
//    cell.contentView.backgroundColor = UIColor.whiteColor;
    cell.model = nil;
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
 return @"删除";//默认文字为 Delete
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
 return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
 if (editingStyle == UITableViewCellEditingStyleDelete) {

 // 删除数据源的数据,self.cellData是你自己的数据
// [self.cellData removeObjectAtIndex:indexPath.row];
// // 删除列表中数据
// [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }

}

@end
