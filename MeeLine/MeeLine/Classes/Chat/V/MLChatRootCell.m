//
//  MLChatRootCell.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLChatRootCell.h"
#import "MLNumberLabel.h"
#import "MLHeadLabel.h"

@interface MLChatRootCell ()
@property (nonatomic ,strong) UIImageView  *headImage;
@property (nonatomic ,strong) MLHeadLabel  *headLabel;
@property (nonatomic ,strong) UILabel      *nameLabel;
@property (nonatomic ,strong) UILabel      *timeLabel;
@property (nonatomic ,strong) UILabel      *detailsLabel;
@property (nonatomic ,strong) UIImageView  *closeImage;
@property (nonatomic ,strong) MLNumberLabel      *numberLabel;
@end


@implementation MLChatRootCell


-(UIImageView*)headImage{
    if (!_headImage) {
        _headImage = [[UIImageView alloc] initWithFrame:CGRectMake(16*kWidth_Scale, 9*kWidth_Scale, 49*kWidth_Scale, 49*kWidth_Scale)];
        _headImage.layer.masksToBounds = YES;
        _headImage.layer.cornerRadius = _headImage.Width/2.0;
        [self.contentView addSubview:_headImage];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(59*kWidth_Scale, self.High-1, self.Width-59*kWidth_Scale, 1)];
        view.backgroundColor = kGrayWhiteColor;
        [self.contentView addSubview:view];
    } return _headImage;
}



-(MLHeadLabel *)headLabel{
    if (!_headLabel) {
        _headLabel = [[MLHeadLabel alloc] initWithFrame:CGRectMake(16*kWidth_Scale, 9*kWidth_Scale, 49*kWidth_Scale, 49*kWidth_Scale)];
        [self.contentView addSubview:_headLabel];
    } return _headLabel;
}


-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headImage.MaxWidth+10*kWidth_Scale, 13*kWidth_Scale,210*kWidth_Scale , 16*kWidth_Scale)];
//        _nameLabel.textAlignment = NSTextAlignmentRight;
        _nameLabel.font = [UIFont systemFontOfSize:16.f*kWidth_Scale];
        _nameLabel.textColor = [UIColor ColorWithHexString:@"#424243"];
        [self.contentView addSubview:_nameLabel];
    } return _nameLabel;
}



-(UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.Width-86*kWidth_Scale, 15*kWidth_Scale,70*kWidth_Scale , 16*kWidth_Scale)];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = [UIFont systemFontOfSize:12.f*kWidth_Scale];
        _timeLabel.textColor = [UIColor ColorWithHexString:@"#98999B"];
        [self.contentView addSubview:_timeLabel];
    } return _timeLabel;
}




-(UILabel *)detailsLabel{
    if (!_detailsLabel) {
        _detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headImage.MaxWidth+10*kWidth_Scale,    8*kWidth_Scale+self.nameLabel.MaxHigh,260*kWidth_Scale , 13*kWidth_Scale)];
        _detailsLabel.font = [UIFont systemFontOfSize:13.f*kWidth_Scale];
        _detailsLabel.textColor = [UIColor ColorWithHexString:@"#98999B"];
        [self.contentView addSubview:_detailsLabel];
    } return _detailsLabel;
}

-(UIImageView*)closeImage{
    if (!_closeImage) {
        _closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.Width-27*kWidth_Scale, 39*kWidth_Scale, 11*kWidth_Scale, 11*kWidth_Scale)];
        [self.contentView addSubview:_closeImage];
    } return _closeImage;;
}

-(MLNumberLabel*)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[MLNumberLabel alloc] initWithFrame:CGRectMake(48*kWidth_Scale, 5*kWidth_Scale, 27*kWidth_Scale, 20*kWidth_Scale)];
        [self.contentView addSubview:_numberLabel];
    } return _numberLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setModel:(MLChatRootModel *)model{
    _model = model;
    
//    self.headImage.image = [UIImage imageNamed:@"icon_head_avatar"];
    self.headLabel.text = @"YH";
    self.nameLabel.text = @"sdahnmbhvgcfhvjbknlmmjhvgcfhvjbknlm";
    self.detailsLabel.text = @"sdafdsghfjmgfhfvbnmjhvgcvjbknjghvjbkcgxcvjbjkjchxgchgvjfcgjhbknjvhj";
    self.timeLabel.text = @"2020/99/99";
    self.closeImage.backgroundColor = [UIColor redColor];
    [self.numberLabel setNumber:@"100"];
}


@end
