//
//  MLChatRootView.h
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLChatRootView : UITableView
-(instancetype) initWithFrame:(CGRect)frame withBlock:(ActionBlock)block;
@end

NS_ASSUME_NONNULL_END
