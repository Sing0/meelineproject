//
//  MLRootListViewCell.h
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLRootListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MLRootListViewCell : UITableViewCell
@property (nonatomic ,strong) MLRootListModel *model;
@end

NS_ASSUME_NONNULL_END
