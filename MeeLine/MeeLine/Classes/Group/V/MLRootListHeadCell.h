//
//  MLRootListHeadCell.h
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLNumberLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MLRootListHeadCell : UIView
@property (nonatomic ,strong) MLNumberLabel      *numberLabel;
-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)title icon:(NSString*)icon;
@end

NS_ASSUME_NONNULL_END
