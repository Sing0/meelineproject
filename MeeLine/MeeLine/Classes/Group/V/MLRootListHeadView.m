//
//  MLRootListHeadView.m
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLRootListHeadView.h"
#import "MLRootListHeadCell.h"

@interface MLRootListHeadView ()
//@property (nonatomic ,strong) NSMutableArray *cellAr;
@end


@implementation MLRootListHeadView



-(instancetype)initWithTitleAr:(NSArray*)titleAr andIconAr:(NSArray*)iocnAr{
    
    CGFloat hi = 40*kWidth_Scale + 52*titleAr.count*kWidth_Scale;
    self = [super initWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH, hi)];
    if (self) {
        UIButton *searchBu = [UIButton buttonWithType:UIButtonTypeCustom];
        searchBu.frame = CGRectMake(0, 0 ,KSCREEN_WIDTH, 50*kWidth_Scale);
        searchBu.backgroundColor =  [UIColor grayColor];
        //        [searchBu setTitle:@"使用其他账号登陆" forState:UIControlStateNormal];
        [self addSubview:searchBu];
        [searchBu addTarget:self action:@selector(searchBuAction) forControlEvents:UIControlEventTouchUpInside];
        for (int i=0; i<titleAr.count; i++) {
            
            MLRootListHeadCell *cell = [[MLRootListHeadCell alloc] initWithFrame:CGRectMake(0, 41*kWidth_Scale+52*kWidth_Scale*i, KSCREEN_WIDTH, 51*kWidth_Scale) title:titleAr[i] icon:iocnAr[i]];
            [self addSubview:cell];
            cell.backgroundColor = [UIColor whiteColor];
            cell.tag = 3000+i;
            UITapGestureRecognizer *ta = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
            [cell addGestureRecognizer:ta];
            
        }
    } return self;
}


-(void)searchBuAction{
    if (self.block) {
        self.block(0, nil);
    }
}


-(void)tapAction:(UITapGestureRecognizer *)tapTwo{
    UIView *iView = (UIView *)tapTwo.view;
    int ta = (int)iView.tag-2999;
    if (self.block) {
        self.block(ta,nil);
    }
}



@end
