//
//  MLRootListView.h
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLRootListView : UITableView
-(instancetype) initWithFrame:(CGRect)frame  tableHeaderView:(UIView*)tableHeaderView withBlock:(ActionBlock)block;
@end

NS_ASSUME_NONNULL_END
