//
//  MLRootListView.m
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLRootListView.h"
#import "MLRootListViewCell.h"


@interface MLRootListView () <UITableViewDelegate,UITableViewDataSource>
@property(nonatomic ,copy)   ActionBlock block;
@end


@implementation MLRootListView


-(instancetype) initWithFrame:(CGRect)frame  tableHeaderView:(UIView*)tableHeaderView withBlock:(ActionBlock)block{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if ( self) {
        self.block = block;
        self.delegate = self;
        self.dataSource = self;
        [self registerClass:[MLRootListViewCell class] forCellReuseIdentifier:@"MLRootListViewCell"];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (tableHeaderView) {
            self.tableHeaderView = tableHeaderView;
        }
    } return self;
}





#pragma mark - SectonHeader
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 24*kWidth_Scale;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.Width, 24*kWidth_Scale)];
    view.backgroundColor = [UIColor ColorWithHexString:@"#F7F7F7"];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*kWidth_Scale, 0, self.Width-30*kWidth_Scale, view.High)];
    //设置 title 文字内容
    titleLabel.text = @"AAAA";
    titleLabel.font = [UIFont systemFontOfSize:13.f*kWidth_Scale];
    //设置 title 颜色
    titleLabel.textColor =  [UIColor ColorWithHexString:@"#A9A9AE"];
    [view addSubview:titleLabel];
    return view;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 63*kWidth_Scale;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MLRootListViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"MLRootListViewCell" forIndexPath:indexPath];
    cell.model = nil;
    return cell;
}



@end
