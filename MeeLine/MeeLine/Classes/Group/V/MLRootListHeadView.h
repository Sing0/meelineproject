//
//  MLRootListHeadView.h
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLRootListHeadView : UIView
@property(nonatomic ,copy)   TypeDataBlock block;
-(instancetype)initWithTitleAr:(NSArray*)titleAr andIconAr:(NSArray*)iocnAr;
@end

NS_ASSUME_NONNULL_END
