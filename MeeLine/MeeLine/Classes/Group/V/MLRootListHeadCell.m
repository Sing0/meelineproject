//
//  MLRootListHeadCell.m
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLRootListHeadCell.h"


@implementation MLRootListHeadCell

-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)title icon:(NSString*)icon{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView *headImage = [[UIImageView alloc] initWithFrame:CGRectMake(15*kWidth_Scale, 6*kWidth_Scale, 41*kWidth_Scale, 41*kWidth_Scale)];
        headImage.layer.masksToBounds = YES;
//         headImage.clipsToBounds  = YES;
        headImage.layer.cornerRadius = headImage.Width/2.0;
        [self addSubview:headImage];
        headImage.backgroundColor = kGrayWhiteColor;
        headImage.image = [UIImage imageNamed:icon];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(66*kWidth_Scale, self.High-1, self.Width-66*kWidth_Scale, 1)];
        view.backgroundColor = kGrayWhiteColor;
        [self addSubview:view];


        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(headImage.MaxWidth+10*kWidth_Scale, 10*kWidth_Scale,150*kWidth_Scale , self.High-20*kWidth_Scale)];
        //        _nameLabel.textAlignment = NSTextAlignmentRight;
        nameLabel.text = title;
        nameLabel.font = [UIFont systemFontOfSize:15.f*kWidth_Scale];
        nameLabel.textColor = [UIColor ColorWithHexString:@"#424243"];
        [self addSubview:nameLabel];

        self.numberLabel = [[MLNumberLabel alloc] initWithFrame:CGRectMake(self.MaxWidth-16*kWidth_Scale-27*kWidth_Scale, 16*kWidth_Scale,27*kWidth_Scale , 20*kWidth_Scale)];
//        self.numberLabel.backgroundColor = kReadColor;
//        self.numberLabel.textAlignment = NSTextAlignmentCenter;
//        self.numberLabel.layer.masksToBounds = YES;
//        self.numberLabel.layer.cornerRadius = 10*kWidth_Scale;
//        self.numberLabel.font = [UIFont systemFontOfSize:15.f*kWidth_Scale];
//        self.numberLabel.textColor = [UIColor whiteColor];
        [self.numberLabel setNumber:@"9"];
        [self addSubview:self.numberLabel];
        
    } return self;
}

@end
