//
//  MLRootListViewCell.m
//  MeeLine
//
//  Created by sing on 2020/4/15.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLRootListViewCell.h"
#import "MLHeadLabel.h"

@interface MLRootListViewCell ()
@property (nonatomic ,strong) UIImageView  *headImage;
@property (nonatomic ,strong) UILabel      *nameLabel;
@property (nonatomic ,strong) MLHeadLabel  *headLabel;
@end


@implementation MLRootListViewCell


-(UIImageView*)headImage{
    if (!_headImage) {
        _headImage = [[UIImageView alloc] initWithFrame:CGRectMake(16*kWidth_Scale, 6*kWidth_Scale, 43*kWidth_Scale, 43*kWidth_Scale)];
        _headImage.layer.masksToBounds = YES;
        _headImage.layer.cornerRadius = _headImage.Width/2.0;
        [self.contentView addSubview:_headImage];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, self.High-1, self.Width, 1)];
        view.backgroundColor = kGrayWhiteColor;
        [self.contentView addSubview:view];
    } return _headImage;
}

-(MLHeadLabel *)headLabel{
    if (!_headLabel) {
        _headLabel = [[MLHeadLabel alloc] initWithFrame:CGRectMake(16*kWidth_Scale, 6*kWidth_Scale, 43*kWidth_Scale, 43*kWidth_Scale)];
        [self.contentView addSubview:_headLabel];
    } return _headLabel;
}

-(UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headImage.MaxWidth+11*kWidth_Scale, 15*kWidth_Scale,260*kWidth_Scale , self.High-30*kWidth_Scale)];
        _nameLabel.font = [UIFont systemFontOfSize:14.f*kWidth_Scale];
        _nameLabel.textColor = [UIColor ColorWithHexString:@"#424243"];
        [self.contentView addSubview:_nameLabel];
    } return _nameLabel;
}


-(void)setModel:(MLRootListModel *)model{
    _model = model;
//    self.headImage.image = [UIImage imageNamed:@"icon_head_avatar"]; // 头像
    self.headLabel.text = @"TX"; // 无头像 名字前两个字母
    self.nameLabel.text = @"史蒂芬霍金后来看sdfgmkl.k,hnfvdsxazxcvbnm,.m/金后来看；";
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
