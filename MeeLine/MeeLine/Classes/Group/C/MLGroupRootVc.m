//
//  MLGroupRootVc.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLGroupRootVc.h"
#import "MLRootListView.h"
#import "MLRootListHeadView.h"

@interface MLGroupRootVc ()
@property (nonatomic ,strong) MLRootListHeadView *headView;
@end

@implementation MLGroupRootVc


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"密群";
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headView = [[MLRootListHeadView alloc] initWithTitleAr:@[@"群通知"] andIconAr:@[@"icon_head_avatar"]];
    MLRootListView *mainView = [[MLRootListView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH, KSCREEN_HEIGHT-kNavi_StaBarHeight-kTabBarHeight) tableHeaderView:self.headView withBlock:^(id data) {
        
    }];
    [self.view addSubview:mainView];
    self.headView.block = ^(int type, id data) {
        NSLog(@"tap==== %d",type);
    };
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
