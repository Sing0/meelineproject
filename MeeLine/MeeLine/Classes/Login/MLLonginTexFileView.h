//
//  MLLonginTexFileView.h
//  MeeLine
//
//  Created by sing on 2020/4/14.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLLonginTexFileView : UIView
@property(nonatomic, strong) UITextField *textFileld;
-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString*)title  placeholder:(NSString*)placeholder;
@end

NS_ASSUME_NONNULL_END
