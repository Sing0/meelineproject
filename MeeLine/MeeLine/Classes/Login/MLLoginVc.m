//
//  MLLoginVc.m
//  MeeLine
//
//  Created by sing on 2020/4/14.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLLoginVc.h"
#import "MLLonginTexFileView.h"


@interface MLLoginVc ()
@property(nonatomic, strong) MLLonginTexFileView *acccountFile;
@property(nonatomic, strong) MLLonginTexFileView *pwdFile;
@end

@implementation MLLoginVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView  *headImage = [[UIImageView alloc] initWithFrame:CGRectMake(KSCREEN_WIDTH/2-42.5*kWidth_Scale, 115*kWidth_Scale, 85*kWidth_Scale, 85*kWidth_Scale)];
    headImage.layer.masksToBounds = YES;
    headImage.layer.cornerRadius = headImage.Width/2.0;
    headImage.image = [UIImage imageNamed:@"icon_head_avatar"];
    [self.view addSubview:headImage];
    
    
    
    _acccountFile =  [[MLLonginTexFileView alloc] initWithFrame:CGRectMake(0, headImage.MaxHigh + 64*kWidth_Scale, KSCREEN_WIDTH, 46*kWidth_Scale) withTitle:@"账户" placeholder:@"请填写密聊号"];
    [self.view addSubview:_acccountFile];
    
    _pwdFile =  [[MLLonginTexFileView alloc] initWithFrame:CGRectMake(0, _acccountFile.MaxHigh + 23*kWidth_Scale, KSCREEN_WIDTH, 46*kWidth_Scale) withTitle:@"密码" placeholder:@"请填写密聊密码"];
    [self.view addSubview:_pwdFile];
    
    
    
    UIButton *otherLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    otherLogin.frame = CGRectMake(38*kWidth_Scale, 16*kWidth_Scale+_pwdFile.MaxHigh, 115*kWidth_Scale, 13*kWidth_Scale);
    otherLogin.titleLabel.font = [UIFont systemFontOfSize:13.f*kWidth_Scale];
    [otherLogin setTitle:@"使用其他账号登陆" forState:UIControlStateNormal];
    [otherLogin setTitleColor:[UIColor ColorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [self.view addSubview:otherLogin];
    [otherLogin addTarget:self action:@selector(otherLoginAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *findPwd = [UIButton buttonWithType:UIButtonTypeCustom];
    findPwd.frame =CGRectMake(KSCREEN_WIDTH-102*kWidth_Scale, 16*kWidth_Scale+_pwdFile.MaxHigh, 60*kWidth_Scale, 13*kWidth_Scale);
    findPwd.titleLabel.font = [UIFont systemFontOfSize:13.f*kWidth_Scale];
    [findPwd setTitle:@"找回密码" forState:UIControlStateNormal];
    [findPwd setTitleColor:[UIColor ColorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [self.view addSubview:findPwd];
    [findPwd addTarget:self action:@selector(findPwdAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(42*kWidth_Scale, _pwdFile.MaxHigh+72*kWidth_Scale, KSCREEN_WIDTH-84*kWidth_Scale, 47*kWidth_Scale);
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:16.f*kWidth_Scale];
    [loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBtn.layer.masksToBounds = YES;
    loginBtn.layer.cornerRadius = 10*kWidth_Scale;
    loginBtn.backgroundColor =  [UIColor ColorWithHexString:@"#32CD5A"];
    [self.view addSubview:loginBtn];
    [loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    

}

-(void)otherLoginAction{
    
}

-(void)findPwdAction{
    
}


-(void)loginAction{
    
    NSString *accoutString = _acccountFile.textFileld.text;
    if (accoutString.length<0.5) {
        
        return;
    }
    
    NSString *pwdString = _pwdFile.textFileld.text;
    
    if (pwdString.length<0.5) {
           
           return;
       }
    
    
    
    
    
    
}

@end
