//
//  MLLonginTexFileView.m
//  MeeLine
//
//  Created by sing on 2020/4/14.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLLonginTexFileView.h"

@implementation MLLonginTexFileView

-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString*)title  placeholder:(NSString*)placeholder{
    self = [super initWithFrame:frame];
    if (self) {
        
        UILabel *titleLa = [[UILabel alloc] initWithFrame:CGRectMake(42*kWidth_Scale, 0, 40*kWidth_Scale, self.High)];
        titleLa.text = title;
        titleLa.textColor = [UIColor ColorWithHexString:@"#555555"];
        titleLa.font = [UIFont systemFontOfSize:16.f*kWidth_Scale];
        [self addSubview:titleLa];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(91*kWidth_Scale, 0, 242*kWidth_Scale, self.High)];
        [self addSubview:view];
        view.layer.masksToBounds = YES;
        view.layer.cornerRadius = 10* kWidth_Scale;
        view.backgroundColor = kGrayWhiteColor;
        
        
        self.textFileld = [[UITextField alloc] initWithFrame:CGRectMake(14*kWidth_Scale, 0, 222*kWidth_Scale, self.High)];
        self.textFileld.placeholder = placeholder;
 
        [view addSubview:self.textFileld];
//        self.inputTextFile.font = [UIFont fontWithName:@"PingFangSC-Medium" size:18*SCALING_RATIO];
//         self.inputTextFile.textColor = HEXCOLOR(0x333333);
        
//        UILabel *placeholderLabel = [self.inputTextFile valueForKey:@"_placeholderLabel"];
//              placeholderLabel.text = placeholder;
//              placeholderLabel.textColor = HEXCOLOR(0xcccccc);
//              placeholderLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16*SCALING_RATIO];
    }
    return self;
}





@end
