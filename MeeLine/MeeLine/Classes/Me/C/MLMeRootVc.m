//
//  MLMeRootVc.m
//  MeeLine
//
//  Created by sing on 2020/4/13.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLMeRootVc.h"
#import "MLMeRootHeadView.h"
#import "MLMeRootell.h"


@interface MLMeRootVc () <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) NSArray           *dataAr;
@property (nonatomic ,strong) MLMeRootHeadView  *headView;
@property (nonatomic ,strong) UITableView       *myTableView;
@end

@implementation MLMeRootVc


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"我的";
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = kGrayWhiteColor;
    self.dataAr = @[@[@"设置"],@[@"icon_Me_MoreSetting"]];
    [self addView];
}


-(void)addView{
    
    
//    NSString *svgName = @"scanning";
//    SVGKImage *svgImage = [SVGKImage imageNamed:svgName];
//    SVGKLayeredImageView *svgView = [[SVGKLayeredImageView alloc] initWithSVGKImage:svgImage];
////    svgView.backgroundColor = clearColor;
//    svgView.frame = CGRectMake(100, 100, 100, 100);
//    [self.view addSubview:svgView];
//    svgView.backgroundColor = [UIColor redColor];
        self.headView = [[MLMeRootHeadView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_HEIGHT, 97*kWidth_Scale)];
        [self.view addSubview:self.headView];
        self.headView.headImage.backgroundColor = [UIColor redColor];
        self.headView.nameLabel.backgroundColor = [UIColor redColor];
        self.headView.detailsLabel.backgroundColor = [UIColor redColor];

        self.myTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.myTableView.frame = CGRectMake(0, 0, KSCREEN_WIDTH, KSCREEN_HEIGHT-kNavi_StaBarHeight-kTabBarHeight);
        self.myTableView.delegate = self;
        self.myTableView.dataSource = self;
        self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
       [self.myTableView registerClass:[MLMeRootell class] forCellReuseIdentifier:@"MLMeRootell"];
        self.myTableView.tableHeaderView = self.headView;
        [self.view addSubview:self.myTableView];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *ar = self.dataAr[0];
    return ar.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45*kWidth_Scale;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MLMeRootell* cell = [tableView dequeueReusableCellWithIdentifier:@"MLMeRootell" forIndexPath:indexPath];
    [cell setTitleString:self.dataAr[0][indexPath.row] iconImage:self.dataAr[1][indexPath.row]];
    return cell;
}


@end
