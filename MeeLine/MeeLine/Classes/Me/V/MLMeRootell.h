//
//  MLMeRootell.h
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLMeRootell : UITableViewCell
-(void)setTitleString:(NSString*)title iconImage:(NSString*)iconString;
@end

NS_ASSUME_NONNULL_END
