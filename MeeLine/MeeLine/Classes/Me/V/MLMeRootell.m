//
//  MLMeRootell.m
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLMeRootell.h"

@interface MLMeRootell ()
@property (nonatomic ,strong) UIImageView  *iconImage;
@property (nonatomic ,strong) UILabel      *titelLabel;
@end


@implementation MLMeRootell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(16*kWidth_Scale, 14*kWidth_Scale, 21*kWidth_Scale, 20*kWidth_Scale)];
        [self.contentView addSubview:self.iconImage];
        
        self.titelLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.iconImage.MaxWidth+10*kWidth_Scale, 5*kWidth_Scale, 150*kWidth_Scale, self.High-10*kWidth_Scale)];
         self.titelLabel.font = [UIFont systemFontOfSize:16.f*kWidth_Scale];
         self.titelLabel.textColor = [UIColor ColorWithHexString:@"#6A7194"];
        [self.contentView addSubview: self.titelLabel];
        
        UIImageView *im = [[UIImageView alloc] initWithFrame:CGRectMake(KSCREEN_WIDTH-15*kWidth_Scale-8*kWidth_Scale, 15*kWidth_Scale, 8*kWidth_Scale, 14*kWidth_Scale)];
        im.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:im];
        
        
    } return self;
}

-(void)setTitleString:(NSString*)title iconImage:(NSString*)iconString{
    
    self.iconImage.image = [UIImage imageNamed:iconString];
    self.titelLabel.text = title;
    
}



@end
