//
//  MLMeRootHeadView.m
//  MeeLine
//
//  Created by sing on 2020/4/16.
//  Copyright © 2020 sing. All rights reserved.
//

#import "MLMeRootHeadView.h"





@implementation MLMeRootHeadView



-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kGrayWhiteColor;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 6*kWidth_Scale, self.Width, 84*kWidth_Scale)];
        view.backgroundColor = [UIColor whiteColor];
        [self addSubview:view];
        self.headImage = [[UIImageView alloc] initWithFrame:CGRectMake(10*kWidth_Scale, 10*kWidth_Scale, 64*kWidth_Scale, 64*kWidth_Scale)];
        [view addSubview:self.headImage];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headImage.MaxWidth+10*kWidth_Scale, 27*kWidth_Scale,220*kWidth_Scale , 13.5*kWidth_Scale)];
        self.nameLabel.font = [UIFont systemFontOfSize:16.f*kWidth_Scale];
        self.nameLabel.textColor = [UIColor ColorWithHexString:@"#424243"];
        [view addSubview:self.nameLabel];
        
        
        self.detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headImage.MaxWidth+10*kWidth_Scale, 9*kWidth_Scale+self.nameLabel.MaxHigh,220*kWidth_Scale , 13*kWidth_Scale)];
        self.detailsLabel.font = [UIFont systemFontOfSize:13.f*kWidth_Scale];
        self.detailsLabel.textColor = [UIColor ColorWithHexString:@"#98999B"];
        [view addSubview:self.detailsLabel];
        
        
        
        UIImageView *im = [[UIImageView alloc] initWithFrame:CGRectMake(KSCREEN_WIDTH-15*kWidth_Scale-8*kWidth_Scale, 37*kWidth_Scale, 8*kWidth_Scale, 14*kWidth_Scale)];
        im.backgroundColor = [UIColor redColor];
        [view addSubview:im];
        
        UITapGestureRecognizer *ta = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [self addGestureRecognizer:ta];
        
    } return self;
}


-(void)tapAction{
    if (self.block) {
        self.block(@"");
    }
}


@end
