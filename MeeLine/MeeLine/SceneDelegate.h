//
//  SceneDelegate.h
//  MeeLine
//
//  Created by sing on 2020/4/11.
//  Copyright © 2020 sing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

